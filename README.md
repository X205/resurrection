# x205 CMS

I have decided to rebuild [XOOPS 2.0.18-1](https://sourceforge.net/projects/xoops/files/XOOPS%20Core%20%28stable%20releases%29/XOOPS%202.0.18.2/) to turn into the ***x205 CMS***. I believe it was last updated in 2008, we now are ending 2018. Things have changed a lot since then. But that was the last toy I really liked playing with.

So my plans are to get [XOOPS 2.0.18-1](https://sourceforge.net/projects/xoops/files/XOOPS%20Core%20%28stable%20releases%29/XOOPS%202.0.18.2/) working as close as it did back then, but with these

## Features
* PHP 7+
* MySQLi and/or PDO

Other things will happen as we go along in development, so keep an eye on the [ISSUES page](https://gitlab.com/X205/resurrection/issues)

and sometimes ...

... there may be some development info inside the [wiki](https://gitlab.com/X205/resurrection/wikis/home).
